# Julia codes for Nonlinear Optimization class A.A.2022/2023
These labs/exercises are parts of Politecnico di Milano Nonlinear Optimization class taught by Prof. Edoardo Amaldi and are originally done in MATLAB, which now are re-written in Julia (just for fun).
# Figures
## Lab 4 (1st for Nonlinear Optimization Part)
### Exercise 1: Linesearch methods and Conjugate Gradient methods
![alt text](Figures/l4e1f1.png)
![alt text](Figures/l4e1f2.png)
### Exercise 2: Molecular Distance Geoometry Problem

## Lab 5
Coming Soon...
