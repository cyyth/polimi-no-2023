using Printf
using Plots;
pythonplot();
using LaTeXStrings

include("./Lib/NOpt.jl")
#=
EXERCISE 1
=#
f_rosenbrock(x) = 100 * (x[2] - x[1]^2)^2 + (1 - x[1])^2

# Optimizing Rosenbrock with steepest gradient method
gd₁ = linesearch(f_rosenbrock, [-2.0; 2.0], 1e-30, 1000, verbose=false)
gd₂ = linesearch(f_rosenbrock, [0.0; 0.0], 1e-30, 1000, verbose=false)
gd₃ = linesearch(f_rosenbrock, [-1.0; 0.0], 1e-30, 1000, verbose=false)

nr₁ = linesearch(f_rosenbrock, [-2.0; 2.0], 1e-30, 1000, method="newton", verbose=false)
nr₂ = linesearch(f_rosenbrock, [0.0; 0.0], 1e-30, 1000, method="newton", verbose=false)
nr₃ = linesearch(f_rosenbrock, [-1.0; 0.0], 1e-30, 1000, method="newton", verbose=false)

fr₁ = conjugateGradient(f_rosenbrock, [-2.0; 2.0], 1e-30, 1000, method="fletcher-reeves", verbose=false)
fr₂ = conjugateGradient(f_rosenbrock, [0.0; 0.0], 1e-30, 1000, method="fletcher-reeves", verbose=false)
fr₃ = conjugateGradient(f_rosenbrock, [-1.0; 0.0], 1e-30, 1000, method="fletcher-reeves", verbose=false)

pr₁ = conjugateGradient(f_rosenbrock, [-2.0; 2.0], 1e-30, 1000, method="polak-ribiere", verbose=false)
pr₂ = conjugateGradient(f_rosenbrock, [0.0; 0.0], 1e-30, 1000, method="polak-ribiere", verbose=false)
pr₃ = conjugateGradient(f_rosenbrock, [-1.0; 0.0], 1e-30, 1000, method="polak-ribiere", verbose=false)

# Plot the data
function sub_contour_plot(res::String, title::String)
    XX = range(-2, 2, length=300)
    YY = range(-10, 5, length=300)
    frp(x, y) = 100 * (y - x^2)^2 + (1 - x)^2
    ZZ = @. frp(XX', YY)

    mk_on = false
    g = contour(XX, YY, ZZ, levels=60, color=:turbo)
    path50 = Meta.parse(@sprintf("g = plot!(%s.Xₖ[1,1:50], %s.Xₖ[2,1:50], markers=%d, color=\"blue\", title = \"%s\")", res, res, mk_on, title))
    path500 = Meta.parse(@sprintf("g = plot!(%s.Xₖ[1,50:500], %s.Xₖ[2,50:500], markers=%d, color=\"green\")", res, res, mk_on))
    path1000 = Meta.parse(@sprintf("g = plot!(%s.Xₖ[1,500:1000], %s.Xₖ[2,500:1000], markers=%d, color=\"red\")", res, res, mk_on))
    eval(path50)
    eval(path500)
    eval(path1000)
    return g
end

tt(res::NamedTuple, x₀::String, name::String) =
    @sprintf("x₀ = %s using %s method (ϵ₅₀₀ = %.2e)", x₀, name, res.error)
gdp1 = sub_contour_plot("gd₁", tt(gd₁, "(-2,2)", "GD"))
gdp2 = sub_contour_plot("gd₂", tt(gd₂, "(0,0)", "GD"))
gdp3 = sub_contour_plot("gd₃", tt(gd₃, "(-1,0)", "GD"))

nrp1 = sub_contour_plot("nr₁", tt(nr₁, "(-2,2)3e)", "Newton"))
nrp2 = sub_contour_plot("nr₂", tt(nr₂, "(0,0) e)", "Newton"))
nrp3 = sub_contour_plot("nr₃", tt(nr₃, "(-1,0)3e)", "Newton"))

frp1 = sub_contour_plot("fr₁", tt(fr₁, "(-2,2)", "FR"))
frp2 = sub_contour_plot("fr₂", tt(fr₂, "(0,0)", "FR"))
frp3 = sub_contour_plot("fr₃", tt(fr₃, "(-1,0)", "FR"))

prp1 = sub_contour_plot("pr₁", tt(pr₁, "(-2,2)", "PR"))
prp2 = sub_contour_plot("pr₂", tt(pr₂, "(0,0)", "PR"))
prp3 = sub_contour_plot("pr₃", tt(pr₃, "(-1,0)", "PR"))

f1 = plot(gdp1, nrp1,
    gdp2, nrp2,
    gdp3, nrp3,
    layout=(3, 2), legend=false, reuse=false)

f2 = plot(frp1, prp1,
    frp2, prp2,
    frp3, prp3,
    layout=(3, 2), legend=false, reuse=false)
display(f1)
display(f2)

#=
EXERCISE 2: Molecular Distance Geometry Problem
=#
