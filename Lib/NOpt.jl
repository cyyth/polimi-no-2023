using Optim
using LinearAlgebra
using Printf

function fd∇(f::Function, x::Vector{Float64}, h::Float64=1e-4)
    n = length(x)
    ∇f = vec(zeros(n, 1))
    for i in 1:n
        δ = zeros(n, 1)
        δ[i] = h
        ∇f[i] = (eval(f)(x + δ) - eval(f)(x)) / h
    end
    return ∇f
end

function fdH(f::Function, x::Vector{Float64}, h::Float64=1e-4)
    n = length(x)
    H = zeros(n, n)
    for i in 1:n
        δᵢ = zeros(n, 1)
        δᵢ[i] = h
        for j in 1:n
            δⱼ = zeros(n, 1)
            δⱼ[j] = h
            H[i, j] = (eval(f)(x + δᵢ + δⱼ) - eval(f)(x + δᵢ) - eval(f)(x + δⱼ) + eval(f)(x)) / (h^2)
        end
    end
    return H
end

function linesearch(f::Function, x₀::Vector{Float64}, ε::Float64,
    max_iterations::Int; method::String="gd", verbose::Bool=false)
    xₖ = x₀
    fₖ = eval(f)(x₀)

    Xₖ = x₀
    Fₖ = [fₖ]

    iter = 0
    error = 1e300

    if method == "fletcher-reeves"
        ∇f = fd∇(f, xₖ)
        dₖ = -∇f
    end

    while error > ε && iter < max_iterations
        iter = iter + 1
        # Finding the optimal step-length (α) and direction (dₖ) then compute xₖ
        if method == "newton"
            dₖ = -fdH(f, xₖ) \ fd∇(f, xₖ)
            α = 1.0
        elseif method == "gd"
            dₖ = -fd∇(f, xₖ) # steepest descent direction
            α = α_opt(f, xₖ, dₖ)
        elseif method == "fletcher-reeves"
            α = α_opt(f, xₖ, dₖ)

            ∇fₚ = ∇f
            ∇f = fd∇(f, xₖ)
            β = (∇f' * ∇f) / (∇fₚ' * ∇fₚ)
            dₖ = -∇f + β[1] * dₖ
        else
            @printf("Method %s does not exist.\n", method)
            return (xₖ=xₖ, fₖ=fₖ, iterations=iter, error=error, Xₖ=Xₖ, Fₖ=Fₖ, exit=0)
        end
        xₖ = xₖ + α * dₖ

        # Evaluating the function and computing the error
        error = norm(fd∇(f, xₖ))
        fₖ = eval(f)(xₖ)
        typeof(error)
        print_iter(fₖ, error, α, verbose)

        # Adding the result to the list
        Xₖ = [Xₖ xₖ]
        Fₖ = [Fₖ fₖ]

    end
    return (xₖ=xₖ, fₖ=fₖ, iterations=iter, error=error, Xₖ=Xₖ, Fₖ=Fₖ, exit=1)
end

function conjugateGradient(f::Function, x₀::Vector{Float64}, ε::Float64,
    max_iterations::Int; method::String="fletcher-reeves", verbose::Bool=false)
    xₖ = x₀
    fₖ = eval(f)(x₀)

    Xₖ = x₀
    Fₖ = [fₖ]

    iter = 0
    error = 1e300

    ∇f = fd∇(f, xₖ)
    dₖ = -∇f

    while error > ε && iter < max_iterations
        iter = iter + 1

        α = α_opt(f, xₖ, dₖ)
        xₖ = xₖ + α * dₖ

        ∇fₚ = ∇f
        ∇f = fd∇(f, xₖ)

        # Evaluating the function and computing the error
        error = norm(∇fₚ)
        fₖ = eval(f)(xₖ)

        if method == "fletcher-reeves"
            β = (∇f' * ∇f) / (∇fₚ' * ∇fₚ)
        elseif method == "polak-ribiere"
            β = (∇f' * (∇f - ∇fₚ)) / (∇fₚ' * ∇fₚ)
        else
            @printf("Method %s does not exist.\n", method)
            return (xₖ=xₖ, fₖ=fₖ, iterations=iter, error=error, Xₖ=Xₖ, Fₖ=Fₖ, exit=0)
        end
        dₖ = -∇f + β[1] * dₖ

        print_iter(fₖ, error, α, verbose)

        # Adding the result to the list
        Xₖ = [Xₖ xₖ]
        Fₖ = [Fₖ fₖ]

    end
    return (xₖ=xₖ, fₖ=fₖ, iterations=iter, error=error, Xₖ=Xₖ, Fₖ=Fₖ, exit=1)
end

function print_iter(fₖ::Float64, error::Float64, α::Float64, verbose::Bool)
    if verbose
        @printf("fₖ : %.3e | Error : %.3e | α : %.3e\n", fₖ, error, α)
    end
end

function α_opt(f::Function, xₖ::Vector{Float64}, dₖ::Vector{Float64})
    g(α) = f(xₖ + α[1] * dₖ)
    gₒₚₜ = optimize(g, [0.0], NelderMead()) # 1-d unconstrained nonlinear optimization
    return Optim.minimizer(gₒₚₜ)[1]
end
